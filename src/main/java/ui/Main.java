package ui;

import database.dao.Dao;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        CreateConnectingWindow.getInstance().open();
    }

    @Override
    public void stop() {
        Dao.closeConnection();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
