package ui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class CreateConnectingWindow {

    @Getter
    private static final CreateConnectingWindow instance = new CreateConnectingWindow();

    private Stage stage;

    void open() {
        stage = new Stage();
        stage.setWidth(270);
        stage.setHeight(120);
        Parent root = ConnectingController.getInstance().getAnchorPane();
        root.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle("Connecting to database");
        stage.getIcons().add(new Image("image/connecting.png"));
        stage.show();
    }

    void closeConnectingWindow() {
        stage.close();
    }
}
