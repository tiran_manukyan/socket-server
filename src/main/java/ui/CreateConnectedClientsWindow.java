package ui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import socket.EchoServer;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CreateConnectedClientsWindow {

    @Getter
    private static final CreateConnectedClientsWindow instance = new CreateConnectedClientsWindow();

    private Stage stage;

    void open() {
        stage = new Stage();
        Parent root = ConnectedClientsController.getBasePane();
        stage.setScene(new Scene(root));
        stage.setTitle("Connected clients");
        stage.getIcons().add(new Image("image/clients.png"));
        stage.setHeight(250);
        stage.setWidth(350);
        stage.setMinHeight(250);
        stage.setMinWidth(350);
        stage.show();

        Thread thread = new Thread(() -> {
            try {
                EchoServer.runServerSocket();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void onWindowClose(Runnable runnable) {
        stage.setOnCloseRequest(event -> runnable.run());
    }

}
