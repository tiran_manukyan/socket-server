package ui;

import database.dao.Dao;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import lombok.Getter;
import util.PropertyManager;

class ConnectingController {

    @Getter
    private static final ConnectingController instance = new ConnectingController();

    @Getter
    private AnchorPane anchorPane;

    private ProgressIndicator progressIndicator;

    private Label lblConnecting;

    private Timeline timelineLabel;

    private CustomButton btnRepeat;

    private CustomButton btnEditProperties;

    private ConnectingController() {
        createLabelConnecting();
        createProgressIndicator();
        createRepeatButton();
        createEditPropertiesButton();
        createAnchorPane();
        createTimeLine();
        setDefaultParams();
        Dao.createEntityManagerFactory(this::setOnSucceeded, this::setOnFailed);
    }

    private void createAnchorPane() {
        anchorPane = new AnchorPane();
        anchorPane.getChildren().addAll(progressIndicator, lblConnecting, btnRepeat, btnEditProperties);
    }

    private void createLabelConnecting() {
        lblConnecting = new Label();

        lblConnecting.setFont(Font.font("Verdana", 25));
        lblConnecting.setEffect(new Reflection());
    }

    private void createProgressIndicator() {
        progressIndicator = new ProgressIndicator();

        progressIndicator.setLayoutX(10);
        progressIndicator.setLayoutY(10);

        progressIndicator.getStyleClass().add("progressIndicator");

        progressIndicator.setPrefSize(60, 60);
    }

    private void createTimeLine() {
        timelineLabel = new Timeline(new KeyFrame(Duration.millis(750), event -> setConnectionTextToLabel()));
        timelineLabel.setCycleCount(Animation.INDEFINITE);
        timelineLabel.play();
    }

    private void setConnectionTextToLabel() {
        if (lblConnecting.getText().contains("..."))
            lblConnecting.setText("Connecting");
        else lblConnecting.setText(lblConnecting.getText() + ".");
    }

    private void setOnFailed() {
        progressIndicator.setVisible(false);
        lblConnecting.setText("Connection Failed!");
        lblConnecting.setTextFill(Color.RED);
        lblConnecting.setLayoutX(15);
        lblConnecting.setLayoutY(32);
        timelineLabel.stop();

        btnRepeat.setVisible(true);
        btnEditProperties.setVisible(true);
    }

    private void createRepeatButton() {
        btnRepeat = new CustomButton();
        btnRepeat.setImage("/image/repeat.png");
        btnRepeat.setLayoutX(180);
        btnRepeat.setVisible(false);

        btnRepeat.setOnAction(event -> repeatConnection());

        btnRepeat.setTooltip("Repeat connection");
    }

    private void createEditPropertiesButton() {
        btnEditProperties = new CustomButton();
        btnEditProperties.setImage("/image/edit.png");
        btnEditProperties.setLayoutX(220);
        btnEditProperties.setVisible(false);

        btnEditProperties.setOnAction(event -> PropertyManager.getInstance().openPropertyFile());

        btnEditProperties.setTooltip("Edit properties");
    }

    private void setOnSucceeded() {
        CreateConnectingWindow.getInstance().closeConnectingWindow();

        CreateConnectedClientsWindow.getInstance().open();
    }

    private void repeatConnection() {
        Dao.createEntityManagerFactory(this::setOnSucceeded, this::setOnFailed);
        setDefaultParams();
        timelineLabel.play();
    }

    private void setDefaultParams() {
        btnRepeat.setVisible(false);
        btnEditProperties.setVisible(false);
        progressIndicator.setVisible(true);
        lblConnecting.setTextFill(Color.BLACK);
        lblConnecting.setText("Connecting");
        lblConnecting.setLayoutX(80);
        lblConnecting.setLayoutY(20);
    }

}
