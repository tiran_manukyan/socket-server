package ui;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConnectedClientsController {

    @Getter
    private static AnchorPane basePane;

    private static TabPane tabPane;

    private static Label lblConnectedClientsCount;

    private static int clientsCount;

    private static String connectedClientsText = " client(s) are connected.";

    private Tab tab;

    private AnchorPane paneInTab;

    private TextArea textArea;

    private Button btnCloseSocket;

    private Runnable onCloseAction;

    private String clientName;

    private Label lblConnectionInfo;

    private int number;

    private int messageCount;

    static {
        basePane = new AnchorPane();
        tabPane = new TabPane();
        lblConnectedClientsCount = new Label();

        basePane.getChildren().addAll(lblConnectedClientsCount, tabPane);

        lblConnectedClientsCount.setLayoutX(10);
        lblConnectedClientsCount.setFont(Font.font(28));
        lblConnectedClientsCount.setTextFill(Color.BLUE);
        lblConnectedClientsCount.setText(clientsCount + connectedClientsText);

        AnchorPane.setTopAnchor(tabPane, 50.0);
        AnchorPane.setLeftAnchor(tabPane, 0.0);
        AnchorPane.setRightAnchor(tabPane, 0.0);
        AnchorPane.setBottomAnchor(tabPane, 0.0);
    }

    public ConnectedClientsController(Runnable onCloseAction) {
        this.onCloseAction = onCloseAction;
        createCloseSocketButton();
        createLabel();
        createTextArea();
        createAnchorPane();
        createTab();
        incrementOrDecrementClientsCount(true);
    }

    private void createTab() {
        tab = new Tab();
        tab.setContent(paneInTab);
        tab.setOnCloseRequest(event -> onCloseAction.run());

        Platform.runLater(() -> tabPane.getTabs().add(tab));

        tab.setOnSelectionChanged(event -> {
            if (tab.isSelected()) {
                tab.setText(clientName);
                messageCount = 0;
            }
        });
    }

    private void createLabel() {
        lblConnectionInfo = new Label();
        lblConnectionInfo.setTextFill(Color.RED);
        lblConnectionInfo.setText("Socket connection was closed.");
        lblConnectionInfo.setVisible(false);
        lblConnectionInfo.setFont(Font.font(16));

        lblConnectionInfo.setLayoutX(110);
        lblConnectionInfo.setLayoutY(10);
    }

    public void socketConnectionClosed() {
        lblConnectionInfo.setVisible(true);
        btnCloseSocket.setDisable(true);
        incrementOrDecrementClientsCount(false);
    }

    private void createTextArea() {
        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setFocusTraversable(false);

        AnchorPane.setTopAnchor(textArea, 40.0);
        AnchorPane.setLeftAnchor(textArea, 0.0);
        AnchorPane.setRightAnchor(textArea, 0.0);
        AnchorPane.setBottomAnchor(textArea, 0.0);
    }

    private void createAnchorPane() {
        paneInTab = new AnchorPane();
        paneInTab.getChildren().addAll(textArea, btnCloseSocket, lblConnectionInfo);
    }

    public void inputText(String text) {
        if (!tab.isSelected()) {
            Platform.runLater(() -> tab.setText(clientName + " (" + ++messageCount + ")"));
        }

        textArea.setText(textArea.getText() + System.lineSeparator() + ++number + ") " + text + "    -   " + getCurrentDate());
        textArea.setScrollTop(Double.MAX_VALUE);
    }

    private String getCurrentDate() {
        return new SimpleDateFormat("dd.MM.yyyy  HH:mm:ss").format(new Date());
    }

    private void createCloseSocketButton() {
        btnCloseSocket = new Button("Close Socket");
        btnCloseSocket.setLayoutX(15);
        btnCloseSocket.setLayoutY(10);
        btnCloseSocket.setFocusTraversable(false);

        btnCloseSocket.setOnAction(event -> onCloseAction.run());
    }

    public void setTextToTab(String value) {
        clientName = value;

        Platform.runLater(() -> tab.setText(clientName));
    }

    private static void incrementOrDecrementClientsCount(boolean increment) {
        Platform.runLater(() -> {
            if (increment) {
                lblConnectedClientsCount.setText(++clientsCount + connectedClientsText);
            } else {
                lblConnectedClientsCount.setText(--clientsCount + connectedClientsText);
            }
        });
    }

}
