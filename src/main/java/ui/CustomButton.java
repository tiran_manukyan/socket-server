package ui;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;

class CustomButton extends Button {

    CustomButton() {
        setOnMouseEntered(event -> setEffect(new DropShadow()));
        setOnMouseExited(event -> setEffect(null));
        setFocusTraversable(false);
        getStyleClass().add("button-transparent-background");
    }

    void setImage(String path) {
        setGraphic(new ImageView(new Image(path)));
    }

    void setTooltip(String value) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(value);
        tooltip.setFont(Font.font(15));
        setTooltip(tooltip);
    }
}
