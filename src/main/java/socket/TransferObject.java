package socket;

import lombok.Getter;
import lombok.Setter;
import module.ActionType;

import java.io.Serializable;

@Getter
@Setter
class TransferObject<T> implements Serializable {
    private static final long serialVersionUID = -6493505406336083890L;

    private ActionType actionType;

    private T data;
}
