package socket;

import util.PropertyManager;

import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {

    public static void runServerSocket() throws Exception {
        ServerSocket serverSocket = new ServerSocket(Integer.parseInt(PropertyManager.getInstance().getSocketPort()));

        while (true) {
            Socket clientSocket = serverSocket.accept();

            ClientService service = new ClientService(clientSocket);
            service.start();
        }
    }
}


