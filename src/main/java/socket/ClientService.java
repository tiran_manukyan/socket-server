package socket;

import database.dao.Dao;
import database.entity.ClientEntity;
import module.ClientObjectConverter;
import module.ActionType;
import ui.ConnectedClientsController;
import ui.CreateConnectedClientsWindow;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

class ClientService extends Thread {

    private final static List<ClientService> services = new ArrayList<>();

    private final ConnectedClientsController controller = createController();

    private final Socket clientSocket;

    private ObjectOutputStream writer;

    private boolean running = true;

    ClientService(Socket socket) {
        clientSocket = socket;
        services.add(this);
        CreateConnectedClientsWindow.getInstance().onWindowClose(() -> services.forEach(ClientService::closeSocket));
    }

    private ConnectedClientsController createController() {
        return new ConnectedClientsController(this::closeSocket);
    }

    @Override
    public void run() {

        try {
            writer = new ObjectOutputStream(clientSocket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(clientSocket.getInputStream());

            while (running) {

                TransferObject readObject = (TransferObject) reader.readObject();

                switch (readObject.getActionType()) {

                    case CLIENT_NAME:
                        controller.setTextToTab((String) readObject.getData());
                        break;
                    case DATA_INPUT:
                        showDataInUI((long) readObject.getData());
                        break;
                    case LIST_OF_ODD_NUMBERS:
                        sendObjectList(ActionType.LIST_OF_ODD_NUMBERS);
                        break;
                    case LIST_OF_EVEN_NUMBERS:
                        sendObjectList(ActionType.LIST_OF_EVEN_NUMBERS);
                        break;
                    case CLOSE: {
                        closeSocket();
                        running = false;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDataInUI(long reader) {
        if (saveToDatabase(reader)) {
            controller.inputText(String.valueOf(reader));
        }
    }

    private boolean saveToDatabase(long value) {
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setNumber(value);
        return Dao.create(clientEntity);
    }

    private void closeSocket() {
        try {
            if (!clientSocket.isClosed()) {
                TransferObject transferObject = new TransferObject();
                transferObject.setActionType(ActionType.CLOSE);
                writer.writeObject(transferObject);
                clientSocket.close();
                controller.socketConnectionClosed();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendObjectList(ActionType actionType) {
        TransferObject<List<ClientData>> transferObject = new TransferObject<>();
        if (actionType == ActionType.LIST_OF_ODD_NUMBERS) {
            transferObject.setData(ClientObjectConverter.getInstance().convertToDto(Dao.getClients(0)));
        } else if (actionType == ActionType.LIST_OF_EVEN_NUMBERS) {
            transferObject.setData(ClientObjectConverter.getInstance().convertToDto(Dao.getClients(1)));
        }

        transferObject.setActionType(ActionType.DATA_LIST);

        try {
            writer.writeObject(transferObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
