package database.dao;

import database.entity.ClientEntity;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import util.PropertyManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Properties;

public class Dao {

    private static EntityManager entityManager;

    public static void createEntityManagerFactory(Runnable isSucceeded, Runnable isFailed) {
        Service<EntityManager> service = new Service<EntityManager>() {

            @Override
            protected Task<EntityManager> createTask() {
                return new Task<EntityManager>() {
                    @Override
                    protected EntityManager call() {
                        return Persistence.createEntityManagerFactory("socketProject", createProperties()).createEntityManager();
                    }
                };
            }
        };

        service.setOnSucceeded(event -> {
            entityManager = service.getValue();
            isSucceeded.run();
        });

        service.setOnFailed(event -> isFailed.run());

        service.start();
    }

    public static List<ClientEntity> getClients(long remainder) {
        return entityManager.createQuery("FROM ClientEntity as client where client.number%2 = :remainder", ClientEntity.class)
                .setParameter("remainder", remainder)
                .getResultList();
    }

    public static boolean create(ClientEntity data) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.persist(data);
        } catch (Exception e) {
            transaction.rollback();
        }
        transaction.commit();
        return true;
    }

    private static Properties createProperties() {
        Properties props = new Properties();

        PropertyManager.getInstance().readProperties();

        props.setProperty("javax.persistence.jdbc.url", "jdbc:mysql://"
                + PropertyManager.getInstance().getIpAddress() + ":"
                + PropertyManager.getInstance().getPort()
                + "/socketProject?createDatabaseIfNotExist=true&amp;autoReconnect=true&amp;useSSL=true");

        props.setProperty("hibernate.connection.password", PropertyManager.getInstance().getPassword());
        props.setProperty("hibernate.connection.username", PropertyManager.getInstance().getUsername());

        return props;
    }

    public static void closeConnection() {
        entityManager.close();
        entityManager.getEntityManagerFactory().close();
    }
}
