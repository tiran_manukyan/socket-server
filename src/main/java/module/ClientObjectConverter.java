package module;

import database.entity.ClientEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import socket.ClientData;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientObjectConverter {

    @Getter
    private static ClientObjectConverter instance = new ClientObjectConverter();

    public ClientEntity convertToEntity(ClientData clientData) {
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setId(clientData.getId());
        clientEntity.setNumber(clientData.getNumber());
        return clientEntity;
    }

    public ClientData convertToDto(ClientEntity clientEntity) {
        ClientData clientData = new ClientData();
        clientData.setId(clientEntity.getId());
        clientData.setNumber(clientEntity.getNumber());
        return clientData;
    }

    public List<ClientData> convertToDto(List<ClientEntity> entities) {
        List<ClientData> dtoList = new ArrayList<>(entities.size());

        entities.forEach(clientEntity -> dtoList.add(convertToDto(clientEntity)));

        return dtoList;
    }
}
