package module;

public enum ActionType {

    DATA_INPUT, LIST_OF_ODD_NUMBERS, LIST_OF_EVEN_NUMBERS, CLOSE, CLIENT_NAME, DATA_LIST
}
