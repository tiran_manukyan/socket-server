package util;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@NoArgsConstructor
public class PropertyManager {

    private String PROPERTY_DIRECTORY_PATH = "C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Local\\ServerSocket\\server\\properties\\";

    @Getter
    private String PROPERTY_FULL_PATH = PROPERTY_DIRECTORY_PATH + "config.properties";

    @Getter
    private String ipAddress = "localhost";

    @Getter
    private String port = "3306";

    @Getter
    private String username = "root";

    @Getter
    private String password = "root";

    @Getter
    private String socketPort = "777";

    @Getter
    private static PropertyManager instance = new PropertyManager();

    private boolean createDirectoryIfNotExist() {
        File theDir = new File(PROPERTY_DIRECTORY_PATH);
        if (!theDir.exists()) {
            return theDir.mkdirs();
        }
        return true;
    }

    public void readProperties() {
        if (createDirectoryIfNotExist()) {
            Properties prop = new Properties();
            try {
                prop.load(new FileReader(PROPERTY_FULL_PATH));

                ipAddress = prop.getProperty("ip");
                port = prop.getProperty("port");
                username = prop.getProperty("username");
                password = prop.getProperty("password");
                socketPort = prop.getProperty("socketPort");

            } catch (Exception ex) {
                prop.setProperty("ip", ipAddress);
                prop.setProperty("port", port);
                prop.setProperty("username", username);
                prop.setProperty("password", password);
                prop.setProperty("socketPort", socketPort);
                try {
                    prop.store(new FileOutputStream(PROPERTY_FULL_PATH), null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void openPropertyFile() {
        Desktop dt = Desktop.getDesktop();
        try {
            dt.open(new File(PROPERTY_FULL_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

